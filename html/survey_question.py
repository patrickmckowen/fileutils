class Survey_question(object):
    
    def __init__(self, typ):
        self.type = typ
        
    def __repr__(self):
        html = E.input({'type': self.type})
        return str(ET.tostring(html, pretty_print=True))

    def __str__(self):
        html = E.input({'type': self.type})
        return str(ET.tostring(html, pretty_print=True))
