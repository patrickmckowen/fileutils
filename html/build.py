from lxml import etree as ET
from lxml.builder import E

# some common inline elements
A = E.a
I = E.i
B = E.b

def CLASS(v):
	# helper function, 'class' is a reserved word
	return {'class': v}

def to_str(content):
    return str(ET.tostring(content, pretty_print=True)))
