from .db import Column, Integer, String, relationship, DateTime, datetime, Binary
from .db import Base

class File(Base):
    #
    __tablename__ = 'file'
    #
    id = Column(Integer(), primary_key=True)
    # Origional File Path
    orig_f_path = Column(String(25))
    # Origional file name
    orig_f_name = Column(String(25))
    # File text (if applicable)
    file_text = Column(Integer())
    # File_Binary
    f_binary = Column(Binary)
    # RELATIONSHIP
    # no foreign key here, it's in the many-to-many table
    # mapped relationship, file_tag_table must already be in scope!
    tags = relationship('Tag', secondary='file_tag', backref='files')
    # Notes
    notes = Column(String(1000))
    # Created
    created_on = Column(DateTime(), default=datetime.now)
    # Updated
    updated_on = Column(DateTime(), default=datetime.now,
                        onupdate=datetime.now)

    def __init__(self, orig_f_path, orig_f_name,
                 file_text, f_binary, tags=[], notes=None):
        self.orig_f_path = orig_f_path
        self.orig_f_name = orig_f_name
        self.file_text = file_text
        self.f_binary = f_binary
        self.tags = tags
        self.notes = notes
        
        
    def __repr__(self):
        return "File: {}".format(self.orig_f_name)
