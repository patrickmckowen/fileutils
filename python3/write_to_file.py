def remove_first_line(content):
    new_string = ''
    for line in content.splitlines()[1:]:
            new_string += line + "\n"
    return(new_string)

def write_out(filename, content):
    with open(filename, 'w') as file:
        file.write(remove_first_line(content))
