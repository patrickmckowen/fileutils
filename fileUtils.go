package fileUtils

import (
	//"fmt"
	"os"
	//"github.com/fatih/color"
	"crypto/sha256"
	"io"
	"log"
	"path/filepath"
	"fmt"
)

type File struct {
	Path	string
	Sha256	string
	Type			string	`json:"fileType"`
	Permission		string	`json:"permission"`
	BootstrapIcon		string	`json:BootstrapIcon"`

}

func New(fpath string) File {
	var f File
	absPath, err := filepath.Abs(fpath)
	if err != nil {
		log.Fatal("Error setting .Path: " + err.Error())
	}

	f.Path = absPath
	f.SetSha256()
	f.setFileType()
	f.setFilePermission()
	f.setBootstrapIcon()
	return f
}

func (f *File) SetSha256() {
	fp, err := os.Open(f.Path)
	if err != nil {
		log.Fatal(err)
	}
	defer fp.Close()

	h := sha256.New()
	if _, err := io.Copy(h, fp); err != nil {
		log.Fatal(err)
	}

	f.Sha256 = fmt.Sprintf("%x", h.Sum(nil))
}



func (f *File) setFileType() {
	fi, err := os.Lstat(f.Path)
	if err != nil {
		log.Fatal(err)
	}

	switch mode := fi.Mode(); {
	case mode.IsRegular():
		f.Type = "regular file"
	case mode.IsDir():
		f.Type = "directory"
	case mode&os.ModeSymlink != 0:
		f.Type = "symbolic link"
	case mode&os.ModeNamedPipe != 0:
		f.Type =  "named pipe"
	}
}

func (f *File) setBootstrapIcon() {
	switch f.Type {
	case "directory":
		f.BootstrapIcon = "glyphicon glyphicon-folder-open"
	case "symbolic link":
		f.BootstrapIcon = "glyphicon glyphicon-link"
	case "named pipe":
		f.BootstrapIcon = "glyphicon glyphicon-filter"

	}

	// .png, .jpeg 	glyphicon glyphicon-picture
	// java fas fa-coffee
	// html fas fa-code
	// git fas fa-code-branch
	// arduino files fas fa-microchip
}
/*
func (fr *FileRepresentation) setMd5Sum() {
	f, err := os.Open(fr.Path)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	h := md5.New()
	io.Copy(h, f)

	fr.Md5Sum = string(h.Sum(nil))
	fmt.Sprintf("%x", h.Sum(nil))
}
*/

func (f *File) setSha256() {
	fp, err := os.Open(f.Path)
	if err != nil {
		log.Fatal(err)
	}
	defer fp.Close()

	h := sha256.New()
	if _, err := io.Copy(h, fp); err != nil {
		log.Fatal(err)
	}

	f.Sha256 = fmt.Sprintf("%x", h.Sum(nil))
}

//
// 0400, 0777, etc.
func (f *File) setFilePermission() {
	fi, err := os.Lstat(f.Path)
	if err != nil {
		log.Fatal(err)
	}
	log.Println(string(fi.Mode().Perm()))
	f.Permission = string(fi.Mode().Perm())
}
