package DetectFileType

import (
	"os"
	"net/http"
	"fmt"
	"io"
)

// GetFileType takes a file path string and 
// returns a string representing the content type
// as descripted in the usual http header
func GetFileType(path string) (string, error) {

	file, err := os.Open(path)
	if err != nil {
		return "", err
	}
	defer file.Close()

	// Only the first 512 bytes are used to sniff the content type.
	// Still works if file is lesss than 512 bytes
	buffer := make([]byte, 512)
	n, err := file.Read(buffer)
	if err != nil && err != io.EOF {
		return "", err
	}

	// Reset the read pointer if necessary.
	file.Seek(0, 0)

	// Always returns a valid content-type and "application/octet-stream" if no others seemed to match.
	contentType := http.DetectContentType(buffer[:n])

	//contentType := http.DetectContentType(buffer)
	return contentType, nil
}
