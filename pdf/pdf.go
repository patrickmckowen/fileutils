package pdf

import (
	"os/exec"
	//"fmt"
	//"os"
	//"github.com/fatih/color"
	//"crypto/sha256"
	//"io"
	//"log"
	"path/filepath"
	//"fmt"
	"gitlab.com/patrickmckowen/fileUtils"
)

type Pdf struct {
	fileUtils.File
	Text	string
}

func New(fpath string) Pdf {
	var pdf Pdf
	absPath, err := filepath.Abs(fpath)
	if err != nil {
		panic(err)
	}

	pdf.Path = absPath
	pdf.SetText()
	pdf.SetSha256()
	return pdf
}

func (pdf *Pdf) SetText() {
	pdf.Text = PdfToText(pdf.Path)
}

func PdfToText(fPath string) string {

    cmd := exec.Command("pdftotext", "-enc", "UTF-8", fPath, "-")
    stdout, err := cmd.Output()

    if err != nil {
       panic(err)
    }

    return string(stdout)
}

