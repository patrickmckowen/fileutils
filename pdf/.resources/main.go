package main

import (
	"os/exec"
	"fmt"
	//"github.com/fatih/color"
)

func main () {
	fmt.Println(PdfToText("test.pdf"))
	//print(PdfToText("test.pdf"))
	//color.Cyan(PdfToText("test.pdf"))
}

func PdfToText(file_path string) string {

    cmd := exec.Command("pdftotext", "-enc", "UTF-8", file_path, "-")
    stdout, err := cmd.Output()

    if err != nil {
        println(err.Error())
        //return
    }

    return string(stdout)
}
