package main

import (
	"fmt"
	"bufio"
	"strings"
	"crypto/sha256"
	"io"
	"log"
	"os"
	"time"
	"encoding/json"
)

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	var line string
	var fi FileInfo
	for scanner.Scan() {
		line = strings.Trim(scanner.Text(), " ")
		fi = NewFileInfo(line)
		//fmt.Printf("%+v\n", fi)
		b, err := json.Marshal(fi)
		fmt.Println(string(b))
		if err != nil {
			log.Fatal(err)
		}
	}
}

type FileInfo struct {
	Path		string `json:"path"`
	FName		string `json:"fName"`
	Size		int64 `json:"size"`
	Mode		os.FileMode `json:"mode"`
	ModTime		time.Time `json:"modTime"`
	IsDir		bool `json:"isDir"`
	Sha256		string `json:"sha256"`
}

func NewFileInfo(fPath string) FileInfo {
	fileStat, _ := os.Stat(fPath)
	fi := FileInfo {
		Path: fPath,
		// Base name of the file
		FName: fileStat.Name(),
		// Length in bytes for regular files
		Size: fileStat.Size(),
		// File mode bits
		Mode: fileStat.Mode(),
		// Last modification time
		ModTime: fileStat.ModTime(),
		// Abbreviation for Mode().IsDir()
		IsDir: fileStat.IsDir(),
		// Get sha256
		Sha256: getSha256(fPath),
	}
	return fi
}


func (fi FileInfo) PrintFileInfo(fPath string) {
	// Base name of the file
	fmt.Println("File Path:", fi.Path)
	// Base name of the file
	fmt.Println("File Name:", fi.FName)
	// Length in bytes for regular files
	fmt.Println("Size:", fi.Size)
	// File mode bits
	fmt.Println("Permissions:", fi.Mode)
	// Last modification time
	fmt.Println("Last Modified:", fi.ModTime)
	// Abbreviation for Mode().IsDir()
	fmt.Println("Is Directory: ", fi.IsDir)
	// Get sha256
	fmt.Println("Sha256: ", fi.Sha256)
}


func getSha256(fPath string) string {
  f, err := os.Open(fPath)
  if err != nil {
    log.Fatal(err)
  }
  defer f.Close()
  h := sha256.New()
  if _, err := io.Copy(h, f); err != nil {
    log.Fatal(err)
  }
  return fmt.Sprintf("%x", h.Sum(nil))
}


